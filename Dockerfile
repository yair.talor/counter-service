FROM python:3
RUN pip install flask
WORKDIR /usr/app/src
COPY counter-service.py ./
EXPOSE 80/tcp
CMD ["python3", "./counter-service.py"]
